This repo includes the code to run https://universis.io

How to install

Install dependencies

npm i

Build it

npm run build

Run it

npm start

See it

http://localhost:8000/

Technologies
Our webpage is built using gatsby